<#--
/****************************************************
 * Description: t_mall_order的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/order/save" id=tabId>
   <input type="hidden" name="id" value="${order.id}"/>
   
   <@formgroup title='订单id'>
	<input type="text" name="orderId" value="${order.orderId}" check-type="required">
   </@formgroup>
   <@formgroup title='实付金额'>
	<input type="text" name="payment" value="${order.payment}" >
   </@formgroup>
   <@formgroup title='支付类型 1在线支付 2货到付款'>
	<input type="text" name="paymentType" value="${order.paymentType}" check-type="number">
   </@formgroup>
   <@formgroup title='邮费'>
	<input type="text" name="postFee" value="${order.postFee}" >
   </@formgroup>
   <@formgroup title='状态 0未付款 1已付款 2未发货 3已发货 4交易成功 5交易关闭 6交易失败'>
	<input type="text" name="status" value="${order.status}" check-type="number">
   </@formgroup>
   <@formgroup title='订单创建时间'>
	<@date name="createTime" dateValue=order.createTime  default=true/>
   </@formgroup>
   <@formgroup title='订单更新时间'>
	<@date name="updateTime" dateValue=order.updateTime  default=true/>
   </@formgroup>
   <@formgroup title='付款时间'>
	<@date name="paymentTime" dateValue=order.paymentTime  default=true/>
   </@formgroup>
   <@formgroup title='发货时间'>
	<@date name="consignTime" dateValue=order.consignTime  default=true/>
   </@formgroup>
   <@formgroup title='交易完成时间'>
	<@date name="endTime" dateValue=order.endTime  default=true/>
   </@formgroup>
   <@formgroup title='交易关闭时间'>
	<@date name="closeTime" dateValue=order.closeTime  default=true/>
   </@formgroup>
   <@formgroup title='物流名称'>
	<input type="text" name="shippingName" value="${order.shippingName}" >
   </@formgroup>
   <@formgroup title='物流单号'>
	<input type="text" name="shippingCode" value="${order.shippingCode}" >
   </@formgroup>
   <@formgroup title='用户id'>
	<input type="text" name="userId" value="${order.userId}" check-type="number">
   </@formgroup>
   <@formgroup title='买家留言'>
	<input type="text" name="buyerMessage" value="${order.buyerMessage}" >
   </@formgroup>
   <@formgroup title='买家昵称'>
	<input type="text" name="buyerNick" value="${order.buyerNick}" >
   </@formgroup>
   <@formgroup title='买家是否已经评价'>
	<input type="text" name="buyerComment" value="${order.buyerComment}" check-type="number">
   </@formgroup>
</@input>