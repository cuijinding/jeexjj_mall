package com.xjj.mall.service;


import java.util.List;

import com.xjj.mall.common.pojo.DataTablesResult;
import com.xjj.mall.entity.PanelContentEntity;
import com.xjj.mall.entity.PanelEntity;
import com.xjj.mall.pojo.AllGoodsResult;
import com.xjj.mall.pojo.ProductDet;


/**
 * @author zhanghejie
 */
public interface ContentService {

    /**
     * 添加板块内容
     * @param tbPanelContent
     * @return
     */
    int addPanelContent(PanelContentEntity tbPanelContent);

    /**
     * 通过panelId获取板块具体内容
     * @param panelId
     * @return
     */
    DataTablesResult getPanelContentListByPanelId(Long panelId);

    /**
     * 删除板块内容
     * @param id
     * @return
     */
    int deletePanelContent(Long id);

    /**
     * 编辑板块内容
     * @param tbPanelContent
     * @return
     */
    int updateContent(PanelContentEntity tbPanelContent);

    /**
     * 通过id获取板块内容
     * @param id
     * @return
     */
    PanelContentEntity getTbPanelContentById(Long id);

    /**
     * 获取首页数据
     * @return
     */
    List<PanelEntity> getHome();

    /**
     * 获取商品推荐板块
     * @return
     */
    List<PanelEntity> getRecommendGoods();

    /**
     * 获取我要捐赠板块
     * @return
     */
    List<PanelEntity> getThankGoods();

    /**
     * 获取商品详情
     * @param id
     * @return
     */
    ProductDet getProductDet(Long id);

    /**
     * 分页多条件获取全部商品
     * @param page
     * @param size
     * @param sort
     * @param priceGt
     * @param priceLte
     * @return
     */
    AllGoodsResult getAllProduct(int page, int size, String sort, Long cid, Long priceGt, Long priceLte);

    /**
     * 获取首页缓存
     * @return
     */
    String getIndexRedis();

    /**
     * 同步首页缓存
     * @return
     */
    int updateIndexRedis();

    /**
     * 获取推荐板块缓存
     * @return
     */
    String getRecommendRedis();

    /**
     * 同步推荐板块缓存
     * @return
     */
    int updateRecommendRedis();

    /**
     * 获取推荐板块缓存
     * @return
     */
    String getThankRedis();

    /**
     * 同步推荐板块缓存
     * @return
     */
    int updateThankRedis();

    /**
     * 获取导航栏
     * @return
     */
    List<PanelContentEntity> getNavList();
}
